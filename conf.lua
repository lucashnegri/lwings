Conf = {
    width  = 650,
    height = 600,
}

Conf.rect = {0, Conf.width, 0, Conf.height}

function love.conf(t)
    t.title  = "lwings"
    t.author = "Lucas Hermann Negri"
    t.url    = "http://oproj.tuxfamily.org"
    t.version = "0.9.0"
    t.release = false
    
    t.window.width      = Conf.width
    t.window.height     = Conf.height
    t.window.fullscreen = false
    t.window.vsync      = true
    t.window.fsaa       = 0
    
    t.modules.joystick  = false
    t.modules.physics   = false
end
