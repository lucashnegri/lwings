Animation   = {}
local AnimationMT = {__index = Animation}

function Animation.new(name, n, dur, fix, back)
    local self = {}
    setmetatable(self, AnimationMT)
    
    self.sprites = {}
    self.acc     = 0
    self.cur     = 1
     
    if not dur then dur = 1e10 end
     
    if type(dur) == 'number' then  
        self.dur = {dur} -- accumulate
        for i = 2, n do self.dur[i] = self.dur[i-1] + dur end
    else
        self.dur = dur
    end
    
    -- compute the shape size and drawing adjustment
    self.w, self.h = self:load_images(name, n)
    self.adj_x     = self.w / 2 - (fix and fix[1] or 0)
    self.adj_y     = self.h / 2 - (fix and fix[2] or 0)
    
    -- special case: back and forth
    if back then self:build_back_anim() end
    
    return self
end

local template = 'res/%s_%d.png'
local sprites  = {}

function Animation:get_sprite(name, i)
    local path = string.format(template, name, i)
    
    if not sprites[path] then
        local sprite  = love.graphics.newImage(path)
        sprites[path] = sprite
    end
    
    return sprites[path]
end

function Animation:load_images(name, n)
    for i = 1, n do
        table.insert(self.sprites, self:get_sprite(name, i))
    end
    
    local s = self.sprites[1]
    return s:getWidth(), s:getHeight()
end

function Animation:update(dt)
    self.acc = self.acc + dt
    
    while self.acc > self.dur[ self.cur ] do
        self.cur = self.cur + 1
        if self.cur > #self.sprites then
            self.cur = 1
            self.acc = self.acc - self.dur[ #self.sprites ]
        end
    end
    
    return self.sprites[ self.cur ]
end

function Animation:build_back_anim()
    local n = #self.sprites
    
    for i = 1, n-1 do
        local pos = n-i+1
        table.insert(self.sprites, self.sprites[pos])
        self.dur[n+i] = self.dur[n+i-1] + (self.dur[pos] - self.dur[pos-1])
    end
end
