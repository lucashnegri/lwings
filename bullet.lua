Bullet    = {}
local BulletMT  = {__index = Bullet}
setmetatable(Bullet, {__index = Actor})

function Bullet.spawn(mng, name, x, y)
    local b = Actor.spawn(mng, name, x, y)
    setmetatable(b, BulletMT)
    
    return b
end

function Bullet.shoot_dir(mng, name, x, y, xdir, ydir)
    local b          = Bullet.spawn(mng, name, x, y)
    b.x_speed        = b.x_speed * xdir
    b.y_speed        = b.y_speed * ydir
    b.script.acts[1] = {at = 0, full = true}
    
    return b
end

function Bullet.shoot_target_dir(mng, name, x, y, tx, ty)
    local b = Bullet.spawn(mng, name, x, y)

    local dx = tx - x
    local dy = ty - y
    local hp = math.sqrt(dx*dx + dy*dy)
    local t  = b.speed / hp
    
    b.x_speed = dx * t
    b.y_speed = dy * t
    b.script.acts[1] = {at = 0, full = true}
end

function Bullet.shoot_point(mng, name, x, y, px, py)
    local b          = Bullet.spawn(mng, name, x, y)
    b.script.acts[1] = {at = 0, x = px, y = py}
    
    return b
end
