HUD = {}
local HUDmt = {__index = HUD}

function HUD.new(player)
    local self = {}
    setmetatable(self, HUDmt)
    self.player = player
    
    self.star_filled = love.graphics.newImage('res/star_filled.png')
    self.star_empty  = love.graphics.newImage('res/star_empty.png')
    
    return self
end

local P = {"I", "II", "III", "IV", "V", "VI"}
P[0] = '-'
local g = love.graphics

function HUD:draw()
    g.setColor ( 0xFF, 0xEE, 0x10, 0xFF )

    -- weapon power    
    local power = self.player.weapon.power
    g.print( string.format('power: %s', P[power]), 20, 10 )
    
    -- ammo
    local gun   = self.player.weapon.ammo
    local bombs = self.player.bomb.ammo
    g.print( string.format('ammo : %3d/%d', gun, bombs), 20, 30 )
    
    -- score
    local score = self.player.score
    g.print( string.format('score: %9d', score), Conf.width - 190, 10 )
    
    -- life
    local life    = math.max(self.player.life, 0)
    local maxlife = self.player.maxlife
    local scale   = 1.4
    
    g.setColor ( 0x00, 0x00, 0x00, 0xFF )
    g.rectangle("fill", 20, 60, maxlife*scale, 5) -- back
    g.setColor ( 0xFF, 0x00, 0x00, 0xFF )
    g.rectangle("fill", 20, 60, life*scale, 5)    -- front
    g.setColor(255, 255, 255)
end

function HUD:stage_start(number)
    g.setColor (0xFF, 0xEE, 0x10, 0xFF)
    local posx, posy = Utils.center(Conf.width, 220), Utils.center(Conf.height, 50)
    g.print( string.format('Stage %02d. Get ready!', number), posx, posy )
    g.setColor(255, 255, 255)
end

function HUD:stage_end(number, nstars)
    g.setColor ( 0xFF, 0xEE, 0x10, 0xFF )
    local posx, posy = Utils.center(Conf.width, 290), Utils.center(Conf.height, 50) - 50    
    g.print( string.format('Stage %02d ended. Score: %d', number, self.player.stage_score), posx, posy )
    g.setColor(255, 255, 255)
    
    local posx = Utils.center(Conf.width, 64 * 3)
    local posy = Utils.center(Conf.height, 64) + 50
        
    for i = 0, 2 do
        local sprite = i < nstars and self.star_filled or self.star_empty
        love.graphics.draw(sprite, posx + i * 64, posy)
    end
end
