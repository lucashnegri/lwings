Utils = {}

function Utils.load_libs()
    require('utils'    )
    require('animation')
    require('actor'    )
    require('actions'  )
    require('bullet'   )
    require('weapon'   )
    require('manager'  )
    require('script'   )
    require('player'   )
    require('stage'    )
    require('conf'     )
    require('hud'      )
    require('audio'    )
    HC = require('hc')
end

function Utils.shallow_copy(tbl)
    local new = {}
    for i, j in pairs(tbl) do new[i] = j end
    return new
end

function Utils.limit(v, min, max)
    if v < min then
        v = min
    elseif v > max then
        v = max
    end
    
    return v
end

function Utils.control(cur, target, limit)
    if not target then return cur end
    local diff = Utils.limit(target - cur, -limit, limit)
    return cur + diff
end

function Utils.inside_rect(w, h, r)
    return w >= r[1] and w <= r[2] and h >= r[3] and h <= r[4] 
end

function Utils.rect_collision(r1, r2)
    return not (r1[1] > r2[2] or r1[2] < r2[1] or r1[3] > r2[4] or r1[4] < r2[3])
end

function Utils.filter(bag, k, v)
    for i = #bag, 1, -1 do
        if bag[i][k] == v then
            bag[i] = bag[#bag]
            bag[#bag] = nil
        end
    end
end

function Utils.copy_array_into(from, to)
    for i = 1, #from do
        to[#to + 1] = from[i]
    end
end

function Utils.load_code(path)
    local f = io.open(path)
    local c = f:read('*a')
    f:close()
    return loadstring(c)
end

function Utils.center(total, size)
    return (total - size) / 2
end

return Utils
