Player = {}
local PlayerMT = {__index = Player}
setmetatable(Player, {__index = Actor})

function Player.new()
    local self = Actor.spawn(nil, 'player', 0, 0)
    setmetatable(self, PlayerMT)
    
    self.mx, self.my = 0, 0
    self.weapon = Weapon.new(1)
    self.bomb   = Bomb.new()
    self.score  = 0
    self.life   = self.maxlife
    
    return self
end

function Player:next_level()
    self.bomb:reset()
    self.weapon:reset(true)
    self.life  = self.maxlife
    self.alive = true
end

local isDown = love.keyboard.isDown

function Player:handle_input()
    if isDown("left" ) then self.mx = self.mx - 1 end
    if isDown("right") then self.mx = self.mx + 1 end
    if isDown("up"   ) then self.my = self.my - 1 end
    if isDown("down" ) then self.my = self.my + 1 end
    
    self.shoot   = isDown("lctrl" )
    self.laybomb = isDown(" ")
end

local limit = Utils.limit

function Player:update(dt)
    self:move(
        limit(self.x + self.x_speed * dt * self.mx, 0, Conf.width),
        limit(self.y + self.y_speed * dt * self.my, 0, Conf.height)
    )
    
    self.mx, self.my = 0, 0 
    
    self.weapon:update(dt)
    self.bomb  :update(dt)
    
    if self.shoot   then self.weapon:shoot(self) end
    if self.laybomb then self.bomb  :shoot(self) end
    
    return Actor.update(self, dt)
end

function Player:damage(dmg)
    self.weapon:reset()
    Actor.damage(self, dmg)
end

function Player:add_score(points)
    self.score       = self.score       + points
    self.stage_score = self.stage_score + points
end

function Player:score_penality()
    self.score = self.score - self.stage_score
    self.score = math.max(0, math.min(self.score - 5000, math.floor(self.score * 0.85)))
end

function Player:collision(other)
    if other.col_dmg and other.cname == 'enemy_plane' then
        other:damage( self .col_dmg )
        self :damage( other.col_dmg )
    end
end
    
Player.death = Actions.explode_big
