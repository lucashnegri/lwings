local stage, player, hud
local paused = false
math.randomseed( os.time() )

function love.load(arg)
    require('utils').load_libs()

    player = Player.new()
    hud    = HUD.new(player)

    stage  = Stage.new(manager, player)
    stage:load(5)

    local font = love.graphics.newFont("res/DejaVuSansMono.ttf", 18)
    love.graphics.setFont(font)
    love.graphics.setBackgroundColor(0x00, 0x43, 0xAB, 0xFF)
end

local time, start_when = 0, -1
local state = 'normal' -- 'start', 'normal', 'won'

function verify_state(name, v)
    if stage[name] then
        stage[name] = false
        start_when = time + v
        state = name
    end
end

local pre = {
    dead = function(dt)
        stage:update(dt)
    end,
}

local post = {
    normal = function(dt)
        player:handle_input()
        stage:update(dt)
    end,

    won = function(dt)
        stage:load( stage.number + 1)
    end,

    dead = function(dt)
        player:score_penality()
        stage:load( stage.number )
    end,
}

local draw_act = {
    start = function()
        hud:stage_start(stage.number)
    end,

    won   = function()
        hud:stage_end  ( stage.number, stage:calc_player_rating() )
    end,
}

function love.update(dt)
    if paused then return end
    time = time + dt
    --io.write(time, '\n')

    -- verify special conditions
    verify_state('start', 1 )
    verify_state('won'  , 10)
    verify_state('dead' , 3)

    -- pre transition action
    if pre[state] then pre[state](dt) end

    -- wait the state transition
    if time < start_when then return end

    -- post transition action
    if post[state] then post[state](dt) end
    state = 'normal'
end

function love.draw()
    if state == 'normal' or state == 'dead' then stage:draw() end
    hud:draw()
    if draw_act[state] then draw_act[state]() end
end

function love.keypressed(k)
    if k == 'escape' then
        love.event.quit()
    elseif k == 'p' then
        paused = not paused
    end
end
