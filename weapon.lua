Weapon = {}
local WeaponMT = {__index = Weapon}

function Weapon.new(level)
    local self = {}
    setmetatable(self, WeaponMT)
    self.max      = 6
    self.power    = Utils.limit(level or 1, 1, self.max)
    self.ct       = 0.25
    self.cd       = self.ct
    self.max_ammo = 200
    self.ammo     = self.max_ammo
    
    return self
end

function Weapon:upgrade(levels)
    self.power = math.min(self.max, self.power + (levels or 1))
    self.ammo  = self.max_ammo
end

function Weapon:reset(mantain_power)
    if not mantain_power then self.power = 1 end
    self.ammo = self.max_ammo
end

function Weapon:update(dt)
    if self.cd > 0 then self.cd = self.cd - dt end
end

function Weapon:shoot(player)
    if self.cd <= 0 then
        if self.ammo > 0 then
            self.cd = self.ct
            self[self.power](self, player)
        end
        
        if self.ammo <= 0 and self.power > 1 then
            self.power = self.power - 1
            self.ammo  = self.max_ammo
        end
    end
end

-- Weapon levels

Weapon[1] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x, p.y - p.gun_y, 0, -1)
    Audio.effect('shoot', 0.3)
end

Weapon[2] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-10, p.y - p.gun_y, 0, -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+10, p.y - p.gun_y, 0, -1)
    self.ammo = self.ammo - 1
    Audio.effect('shoot', 0.4)
end

Weapon[3] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-12, p.y - p.gun_y, 0 , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x   , p.y - p.gun_y    , 0 , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+12, p.y - p.gun_y, 0 , -1)
    self.ammo = self.ammo - 2
    Audio.effect('shoot', 0.5)
end

Weapon[4] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-5, p.y - p.gun_y + 3, -0.05 , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x  , p.y - p.gun_y    , 0     , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+5, p.y - p.gun_y + 3, 0.05  , -1)
    self.ammo = self.ammo - 2
    Audio.effect('shoot', 0.6)
end

Weapon[5] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-20, p.y - p.gun_y + 3, -0.05, -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-10, p.y - p.gun_y    , 0    , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+10, p.y - p.gun_y    , 0    , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+20, p.y - p.gun_y + 3, 0.05 , -1)
    self.ammo = self.ammo - 3
    Audio.effect('shoot', 0.7)
end

Weapon[6] = function(self, p)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-25, p.y - p.gun_y + 6, -0.08, -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x-15, p.y - p.gun_y + 3, -0.04, -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x   , p.y - p.gun_y    , 0    , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+15, p.y - p.gun_y + 3, 0.04 , -1)
    Bullet.shoot_dir(p.mng, 'player_bullet', p.x+25, p.y - p.gun_y + 6, 0.08 , -1)
    self.ammo = self.ammo - 4
    Audio.effect('shoot', 0.8)
end

Bomb = {}
setmetatable(Bomb, WeaponMT)
local BombMT = {__index = Bomb}

function Bomb.new()
    local self = {}
    setmetatable(self, BombMT)
    self.power    = 1
    self.ct       = 1
    self.cd       = self.ct
    self.max_ammo = 3
    self.ammo     = self.max_ammo
    
    return self
end

Bomb[1] = function(self, p)
    Bullet.shoot_dir(p.mng, 'bomb', p.x, p.y, 0, -1)
    self.ammo = self.ammo - 1
end
