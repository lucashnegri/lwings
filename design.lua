-- ** Stage script commands (helps the level designer) **
local w, h          = Conf.width, Conf.height
local sin, pi       = math.sin, math.pi
local print, random = print, math.random
local Design        = {}

function Design.spawn(name, x, y, d)
    x = x * w
    y = (y or -0.2) * h
    acts[#acts+1] = {acc_wait, obj = name, x = x, y = y, d = d}
    acc_wait = 0
end

function Design.wait(t)
    acc_wait = acc_wait + t
end

function Design.win()
   acts[#acts+1] = {acc_wait, action = 'win'}
   acc_wait = 0
end

function Design.block4(e, x)
    spawn(e, x        , -0.2)
    spawn(e, x + 0.12 , -0.2)
    spawn(e, x + 0.06 , -0.26)
    spawn(e, x + 0.18 , -0.26)
end

function Design.wing3b(e, x)
    spawn(e, x      , -0.3)
    spawn(e, x -0.15, -0.12)
    spawn(e, x +0.15, -0.12)
end

function Design.wing3(e, x)
    spawn(e, x      , -0.2)
    spawn(e, x -0.05, -0.25)
    spawn(e, x +0.05, -0.25)
end

function Design.pascalb(e, x)
    wing3b(e, x)
    wait(14)
    wing3b(e, x - 0.25)
    wing3b(e, x + 0.25)
end

function Design.pascal(e, x)
    wing3(e, x)
    wait(3)
    wing3(e, x-0.15)
    wing3(e, x+0.15)
end

function Design.line5(e, x, s)
    s = s or 0.1
    for i = 1, 5 do
        spawn(e, x + (3-i)*s)
    end
end

function Design.col3(e, x, s)
    s = s or 0.1
    for i = 1, 3 do
        spawn(e, x, -i*s)
    end
end

function Design.ladder5e(e, x, s)
    s = s or 0.1
    for i = 1, 5 do
        spawn(e, x + (3-i)*s, -0.1 - i*s)
    end
end

function Design.ladder5d(e, x, s)
    s = s or 0.1
    for i = 1, 5 do
        spawn(e, x + (3-i)*s, -0.1 - (6-i)*s)
    end
end

for i, j in pairs(Design) do
    if type(j) == 'function' then setfenv(j, Design) end
end

-- no need to change the env
for i, j in pairs(math) do Design[i] = j end

return Design
