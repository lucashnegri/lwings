Script = {}
local ScriptMT = {__index = Script}

function Script.new(acts)
    local self = {}
    setmetatable(self, ScriptMT)
    
    self.acts = acts or {}
    self:calc_at()
    
    self.acc  = 0
    self.idx  = 0
    
    return self
end

function Script:calc_at()
    local a = self.acts
    if a[1] then a[1].at = a[1][1] end
    
    for i = 2, #a do
        a[i].at = a[i-1].at + a[i][1]
    end
end

function Script:update(dt)
    self.acc      = self.acc + dt
    local idx     = self.idx
    local changed = false
    
    if idx < #self.acts and self.acc > self.acts[idx+1].at then
        idx = idx + 1
        changed = true
    end
    
    self.idx = idx
    return self.acts[idx], changed
end

function Script:get()
    return self.acts[ self.idx ]
end
