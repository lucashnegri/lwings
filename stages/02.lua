-- ** Part I **

-- islands
spawn('isl3', 0.75,  0.2)
spawn('isl2', 0.55,  0.1)
spawn('isl3', 0.15,  -0.3)
spawn('isl3', 0.3 , -0.6)
spawn('isl3', 0.2 , -1)
spawn('isl3', 0.4 , -1.5)
spawn('isl2', 0.7 , -2)
spawn('isl3', 0.35, -2.5)
spawn('isl2', 0.8 , -3)

-- enemies
wait(1.5)
line5('ep1', 0.5)
wait(5)
pascal('ep1', 0.5)

wait(7)
spawn('ep_dir', 0.2)
spawn('ep_esq', 0.8)
wait(1)
spawn('ep_dir', 0.2)
spawn('ep_esq', 0.8)
wait(1)
spawn('ep_dir', 0.2)
spawn('ep_esq', 0.8)

line5('ep1', 0.5)
wait(3)
spawn('ep2', 0.3)
wait(1)
spawn('ep2', 0.5)
wait(1)
spawn('ep2', 0.7)
wait(1)

block4('ep1', 0.3)
wait(3)
block4('ep1', 0.7)
wait(3)
line5('ep1', 0.5)
wait(5)
col3('ep_dir', 0.7)
spawn('ep1', 0.4)
wait(1)
spawn('ep1', 0.45)
wait(2)
col3('ep_esq', 0.3)
spawn('ep1', 0.55)
wait(1)
spawn('ep1', 0.54)

wait(5)
pascal('ep1', 0.5)
wait(5)

spawn('ep2', 0.3)
spawn('ep1', 0.4)
wait(2)
spawn('ep2', 0.6)
spawn('ep1', 0.7)
wait(2)
spawn('ep2', 0.5)
spawn('ep1', 0.6)
wait(4)

ladder5d('ep1', 0.3)
wait(3)
ladder5e('ep1', 0.7)
wait(3)
wing3('ep2', 0.5)

spawn('ep_dir', 0.6)
wait(1)
spawn('ep1', 0.4)
wait(1)
spawn('ep_esq', 0.4)
wait(1)
spawn('ep1', 0.5)
spawn('ep1', 0.55)
spawn('ep2', 0.6)
wait(3)
block4('ep1', 0.3)
wait(3)
block4('ep1', 0.2)
wait(3)
col3('ep1', 0.7)
wait(2)
col3('ep1', 0.8)
wait(4)
col3('ep1', 0.6)
wait(5)
spawn('ep2', 0.2)
wait(1)
spawn('ep2', 0.3)
wait(5)

-- ** Part I **

-- islands
spawn('isl1', 0.4, -0.2)
spawn('isl2', 0.9, -0.6)
spawn('isl1', 0.7, -1)
spawn('isl3', 0.2, -1.4)
spawn('isl3', 0.5 ,-1.7)
spawn('isl2', 0.1 ,-2)
spawn('isl1', 0.7 ,-2.5)

-- enemies
line5('ep1', 0.5)
wait(5)
line5('ep1', 0.5)
wait(5)
line5('ep1', 0.5)
spawn('ep2', 0.4)
spawn('ep2', 0.6)

wait(3)

for i = 1, 12 do
    spawn('ep_dir', 0.3 + i * 0.03)
    
    if i % 3 == 0 then
        spawn('ep1', 0.15, -0.2)
        spawn('ep1', 0.3,  -0.4)
    end
    
    if i % 4 == 0 then spawn('ep2', 0.5) end
    wait(1.5)
end

wait(4)

for i = 1, 12 do
    spawn('ep_esq', 0.7 - i * 0.03)
    
    if i % 3 == 0 then
        spawn('ep1', 0.8, -0.2)
        spawn('ep1', 0.65, -0.4)
    end
    
    if i % 4 == 0 then spawn('ep2', 0.5) end
    wait(1.5)
end

wait(2)
pascal('ep1', 0.4)
wait(5)
pascal('ep1', 0.6)
wait(5)
col3('ep_dir', 0.5)
wait(3)
col3('ep_esq', 0.5)
spawn('ep2', 0.3)
spawn('ep2', 0.7)
wait(4)
line5('ep1', 0.5)

wait(10)
win()

