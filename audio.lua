-- audio manager

Audio = {}

local EffectCache = {}

function Audio.effect(name, vol)
    if not EffectCache[name] then
        EffectCache[name] = love.audio.newSource( string.format('res/%s.wav', name), "static" )
    end
    
    vol = vol or 1
    local src = EffectCache[name]
    src:rewind()
    src:setVolume(vol)
    src:play()
end
