-- ** power-up deliver class **
local Deliver   = {}
local DeliverMT = {__index = Deliver}
function Deliver.new()
    local self = {}
    setmetatable(self, DeliverMT)
    self.mult  = mult or 1
    self.limit = 15
    self.acc   = 0

    return self
end

function Deliver:verify(w, dt)
    if w.max == 1 then return false end

    local rate = (w.max - w.power) / (w.max-1)
    self.acc   = self.acc + dt * rate

    if self.acc > self.limit then
        self.acc = self.acc - self.limit
        return true
    end

    return false
end

-- ** Stage class **
Stage = {}
local StageMT = {__index = Stage}

function Stage.new(manager, player)
    local self = {}
    setmetatable(self, StageMT)

    self.manager = ActorManager.new()
    self.player  = player
    self.deliver = Deliver.new()

    return self
end

local Design = require('design')

function Stage:load(number)
    local types = require('stages/types')

    -- load the stage actions
    local code      = Utils.load_code(string.format('stages/%02d.lua', number))
    Design.acts     = {}
    Design.acc_wait = 0
    setfenv(code, Design)
    code()

    -- prepare the actor manager
    self.manager:prepare(types)
    self.script = Script.new(Design.acts)
    self.number = number

    -- prepare the player
    self.manager:add_player(self.player)
    self.player.stage_score = 0
    self.player:next_level()

    self.total_score = 0
    self.won   = false
    self.start = true
    self.dead  = false
end

function Stage:update(dt)
    -- verify pending actions
    local adt = dt
    repeat
        local act, changed = self.script:update(adt)
        if changed then self:perform(act) end
        adt = 0
    until not changed

    -- power-up delivery service!
    if self.deliver:verify(self.player.weapon, dt) then
        local w    = Conf.width
        local posx = math.random(w*0.1, w*0.9)
        Actor.spawn(self.manager, 'ep_up', posx, Conf.height * 1.1)
    end

    self.manager:update(dt)

    if not self.player.alive then
        self.dead = true
        self.player.alive = true
    end
end

function Stage:perform(act)
    -- spawn
    if act.obj then
        local actor = Actor.spawn(self.manager, act.obj, act.x, act.y)
        if actor.points then self.total_score = self.total_score + actor.points end
        if act.d then actor.cd = act.d end
    end

    -- next level / end game
    if act.action == 'win' then
        self.won = true
    end
end

function Stage:draw()
    self.manager:draw()
end

function Stage:calc_player_rating()
    local rate = self.player.stage_score / self.total_score
    if rate < 0.5 then return 0 end
    if rate < 0.8 then return 1 end
    if rate < 0.9 then return 2 end
    return 3
end

