return {
    anim = {
        name = 'exp1',
        n    = 6,
        dur  = 0.08
    },

    script = { {0, y = 1e10} },
    
    x_speed = 0,
    y_speed = 20,
    tname   = 'exp_small',
    tmax    = 0.48
}
