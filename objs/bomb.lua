local inside = Utils.inside_rect
local rect   = Conf.rect

return {
    anim = {
        name = 'bomb',
        n    = 1,
        dur  = 1,
        fix  = {-1, 0}
    },
    
    x_speed = 250,
    y_speed = 250,
    
    poly    = {0,0, 0,20, 8,20, 8,0},
    tname   = 'bomb',         
    cname   = 'player_bomb',         
    tmax    = 0.8,                   
    passive = true,
    
    update = function(self, dt)
        if not inside(self.x, self.y, rect) then
            self.alive = false
        end
        
        return Bullet.update(self, dt)
    end,
    
    collision = function(self, other)
        if other.cname == 'enemy_plane' then
            other:damage(self.dmg)
            Actions.exp_tiny(self)
            self.alive = false
        end
    end,
    
    death = function(self)
        Actor.spawn(self.mng, 'bomb_radius', self.x, self.y)
        Actions.explode_big(self)
    end,
}
