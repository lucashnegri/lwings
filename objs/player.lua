return {
    anim = {
        name = 'plane_a',
        n    = 3,
        dur  = 0.05,
        fix  = {-1, -2},
    },
    
    x_speed = 250,
    y_speed = 200,
    
    poly    = {0,0, 0,43, 57,43, 57,0},
    tname   = 'player_plane',
    cname   = 'player_plane',
    
    maxlife = 100,
    life    = 100,
    col_dmg = 50,
    gun_y   = 20,
}
