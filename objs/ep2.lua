return {
    anim = {
        name = 'plane_c',
        n    = 3,
        dur  = 0.025
    },
    
    script = { {0, y = 1e10} },
    
    x_speed = 100,
    y_speed = 150,
    
    poly    = {0,0, 0,30, 29,30, 29,0},
    passive = true,
    
    tname   = 'ep2',
    cname   = 'enemy_plane',
    
    col_dmg = 60,
    points  = 350,
    drop    = 0.1,
    
    tmax    = 20,
    life    = 100,
    
    death = Actions.explode_small
}
