return {
    anim = {
        name = 'plane_e',
        n    = 3,
        dur  = 0.05
    },
    
    script = { {0, y = -1e10} },
    
    x_speed = 50,
    y_speed = 50,
    
    gun_y   = 12,
    poly    = {0,0, 0,30, 29,30, 29,0},
    passive = true,
    
    tname   = 'ep_up',
    cname   = 'enemy_plane',
    
    tmax    = 20,
    life    = 80,
    
    cdv     = 3,
    drop    = 1,
    
    death = Actions.explode_small
}
