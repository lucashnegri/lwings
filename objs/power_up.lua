local inside = Utils.inside_rect
local rect   = Utils.rect

return {
    anim = {
        name = 'power_up',
        n    = 2,
        dur  = 0.5,
    },
    
    x_speed = 0,
    y_speed = 20,
    
    script = { {0, y = 1e10} },
    
    poly    = {0,0, 0,25, 18,25, 18,0},
    passive = true,
    
    tname   = 'power_up',
    cname   = 'power_up',
    
    tmax    = 50,
    
    collision = function(self, other)
        if other.cname == 'player_plane' then
            other.weapon:upgrade()
            self.alive = false
        end
    end,
}
