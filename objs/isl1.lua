return {
    anim = {
        name = 'island1',
        n    = 1,
        dur  = 1
    },

    script = { {0, y = 1e10} },
    
    x_speed = 0,
    y_speed = 20,
    
    tname   = 'isl1',
    update = Actions.desapear,
}
