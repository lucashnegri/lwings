return {
    anim = {
        name = 'exp0',
        n    = 6,
        dur  = 0.04
    },

    script = { {0, y = 1e10} },
    
    x_speed = 0,
    y_speed = 20,
    tname   = 'exp_tiny',
    tmax    = 0.24
}
