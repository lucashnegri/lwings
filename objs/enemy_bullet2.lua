local inside = Utils.inside_rect
local rect   = Conf.rect

return {
    anim = {
        name = 'bullet_b',
        n    = 2,
        dur  = 0.2,
    },
    
    speed   = 150,
    
    poly    = {0,0, 0,5, 5,5, 5,0},
    passive = true,
    
    tname   = 'enemy_bullet2',
    cname   = 'enemy_bullet', 
    tmax    = 4,
    dmg     = 10,
    
    update = function(self, dt)
        if not inside(self.x, self.y, rect) then
            self.alive = false
        end
        
        return Bullet.update(self, dt)
    end,
    
    collision = function(self, other)
        if other.cname == 'player_plane' then
            other:damage(self.dmg)
            Actions.exp_tiny(self)
            self.alive = false
        end
    end
}
