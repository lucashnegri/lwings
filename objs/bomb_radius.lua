local is_target = {
    enemy_plane = true,
    sub         = true
}

return {
    x_speed = 0,
    y_speed = 0,
    
    circle  = {0, 0, 250},
    tname   = 'bomb_radius',  
    cname   = 'bomb_radius',  
    tmax    = 0.075,                   
    dmg     = 300,
    
    collision = function(self, other)
        if is_target[other.cname] then
            if other:damage(self.dmg) then
                Actions.exp_tiny(other)
            end
            
            self.alive = false
        end
    end,
    
    death = function(self)
        Actions.exp_big(self)
        return Actor.death(self)
    end
}
