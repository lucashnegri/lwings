return {
    anim = {
        name = 'sub',
        n    = 6,
        dur  = {2, 2.15, 2.3, 2.45, 2.6, 4},
        back = true,
    },
    
    script = { {0, y = 1e10} },
    
    x_speed = 0,
    y_speed = 20,
    
    gun_y   = 10,
    poly    = {0,0, 0,90, 21,90, 21,0},
    passive = true,
    
    tname   = 'sub',
    cname   = 'sub',
    
    tmax    = 30,
    life    = 150,
    
    col_dmg = 0,
    points  = 2000,
    
    cdv     = 0.4,
    
    above_water = function(self)
        local cur = self.anim.cur
        return cur == 1 or cur == 11
    end,
    
    damage = function(self, qt)
        if self:above_water() then
            return Actor.damage(self, qt)
        else
            return false
        end
    end,
    
    update = function(self, dt)
        if self:above_water() then
            Actions.shoot_player(self, dt)
        else
            self.cd = self.cdv
        end
        
        return Actor.update(self, dt)
    end,
    
    collision = function(self, other)
        if self:above_water() and other.cname == 'player_bullet' then
            self:damage(other.dmg)
            Actions.exp_tiny(other)
            other.alive = false
        end
    end,
    
    death = Actions.explode_big
}
