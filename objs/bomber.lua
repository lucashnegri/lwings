return {
    anim = {
        name = 'plane_bomber',
        n    = 3,
        dur  = 0.075,
        fix  = {0, 5}
    },
    
    script = { {0, y = 1e10} },
    
    x_speed = 50,
    y_speed = 15,
    
    gun_y   = 15,
    poly    = {0,0, 0,25, 30,25, 30,65, 60,65, 60,25, 90,25, 90,0},
    passive = true,
    
    tname   = 'bomber',
    cname   = 'enemy_plane',
    
    col_dmg = 300,
    points  = 3000,
    drop    = 0.5,
    
    tmax    = 80,
    life    = 800,
    
    cdv     = 0.75,
    
    update = function(self, dt)
        Actions.shoot_player(self, dt)
        return Actor.update(self, dt)
    end,
    
    death = Actions.explode_big
}
