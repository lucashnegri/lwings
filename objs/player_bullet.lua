local inside = Utils.inside_rect
local rect   = Conf.rect

return {
    anim = {
        name = 'bullet_a',
        n    = 1,
        dur  = 1,
        fix  = {-1, 0}
    },
    
    x_speed = 500,
    y_speed = 500,
    
    poly    = {0,0, 0,20, 8,20, 8,0},   -- bounding polygon to handle collisions
    tname   = 'player_bullet',          -- type name
    cname   = 'player_bullet',          -- class name
    tmax    = 2,                        -- maximum living time
    dmg     = 20,                       -- damage
    player  = true,                     -- object controlled by a player
    
    update = function(self, dt)
        if not inside(self.x, self.y, rect) then
            self.alive = false
        end
        
        return Bullet.update(self, dt)
    end,
    
    collision = function(self, other)
        if other.cname == 'enemy_plane' then
            other:damage(self.dmg)
            Actions.exp_tiny(self)
            self.alive = false
        end
    end
}
