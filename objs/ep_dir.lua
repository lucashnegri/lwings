return {
    anim = {
        name = 'plane_d',
        n    = 8
    },
    
    script = {
        {0  , y = 1e10            , sprite = 1},
        {2  , x = 1e10, y = 1e10  , sprite = 2},
        {1.5, y = 1e10            , sprite = 1},
        {2  , x = -1e10 , y = 1e10, sprite = 8},
        {1.5, y = 1e10            , sprite = 1},
    },
    
    x_speed = 90,
    y_speed = 90,
    
    gun_y   = 12,
    poly    = {0,0, 0,30, 29,30, 29,0},
    passive = true,
    
    tname   = 'ep_dir',
    cname   = 'enemy_plane',
    
    tmax    = 20,
    life    = 60,
    
    col_dmg = 50,
    points  = 500,
    drop    = 0.02,
    
    cdv     = 1.5,
    
    update = function(self, dt)
        local act = self.script:get()
        if act then self.anim.cur = act.sprite end
        
        Actions.shoot_simple(self, dt)
        return Actor.update(self, dt)
    end,
    
    death = Actions.explode_small
}
