return {
    anim = {
        name = 'plane_b',
        n    = 3,
        dur  = 0.05
    },
    
    script = { {0, y = 1e10} },
    
    x_speed = 50,
    y_speed = 50,
    
    gun_y   = 12,
    poly    = {0,0, 0,30, 29,30, 29,0},
    passive = true,
    
    tname   = 'ep1',
    cname   = 'enemy_plane',
    
    tmax    = 20,
    life    = 40,
    
    col_dmg = 40,
    points  = 100,
    drop    = 0.02,
    
    cdv     = 3,
    
    update = function(self, dt)
        Actions.shoot_simple(self, dt)
        return Actor.update(self, dt)
    end,
    
    death = Actions.explode_small
}
