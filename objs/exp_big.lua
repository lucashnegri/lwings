return {
    anim = {
        name = 'exp2',
        n    = 7,
        dur  = 0.08
    },

    script = { {0, y = 1e10} },
    
    x_speed = 0,
    y_speed = 20,
    tname   = 'exp_big',
    tmax    = 0.56
}
