local inside = Utils.inside_rect
local rect   = Conf.rect

return {
    anim = {
        name = 'bullet_b',
        n    = 2,
        dur  = 0.2,
    },
    
    x_speed = 250,
    y_speed = 250,
    
    poly    = {0,0, 0,5, 5,5, 5,0},
    passive = true,
    
    tname   = 'enemy_bullet',
    cname   = 'enemy_bullet', 
    tmax    = 3,
    dmg     = 20,
    
    update = function(self, dt)
        if not inside(self.x, self.y, rect) then
            self.alive = false
        end
        
        return Bullet.update(self, dt)
    end,
    
    collision = function(self, other)
        if other.cname == 'player_plane' then
            other:damage(self.dmg)
            Actions.exp_tiny(self)
            self.alive = false
        end
    end
}
