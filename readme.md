# lwings

## About

*lwings* is a vertical shooter featuring planes. It is my first game, and it is being written
for educational (my education!) purposes.

## Dependencies

love2d 0.9.0

## Contact

* e-mail: lucashnegri@gmail.com
* web: http://oproj.tuxfamily.org

## Licence

GPL 3+
