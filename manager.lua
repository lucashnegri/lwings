ActorManager = {}
local ActorManagerMT = {__index = ActorManager}

function ActorManager.new()
    local self = {}
    setmetatable(self, ActorManagerMT)
    
    return self
end

-- prepares the types / ztable
function ActorManager:prepare(types)
    self.types = Utils.shallow_copy(types)
    table.sort(self.types, function(a, b)
        return a.zorder < b.zorder
    end)
    
    self.objs  = {}
    self.queue = {}
    
    for i = 1, #types do
        local type = types[i]
        self.objs[type.name] = {}
    end
    
    self.hc = HC(64, function (dt, a, b)
        a.actor:collision(b.actor)
        b.actor:collision(a.actor)
    end)
end

-- cals func for every managed actor
function ActorManager:foreach_actor(func)
    for i = 1, #self.types do
        local typename  = self.types[i].name
        local typetable = self.objs[typename]
        
        for i, obj in pairs(typetable) do
            func(obj, i, typetable)
        end
    end
end

function ActorManager:update(dt)
    -- add new actors in queue
    for i = 1, #self.queue do
        local obj = self.queue[i]
        table.insert(self.objs[obj.tname], obj)
        self.queue[i] = nil
    end

    -- update actors (destrois and releases dead actors too)
    self:foreach_actor(
        function(obj, i, typetable)
            local alive = obj:update(dt, self)
            
            if not alive then
                typetable[i] = nil
                obj:death(self)
                if obj.shape then self.hc:remove(obj.shape) end
            end
        end
    )
    
    -- update the collision detection system
    self.hc:update(dt)
end

function ActorManager:draw()
    self:foreach_actor(Actor.draw)
end

function ActorManager:add(actor)
    table.insert(self.queue, actor)
    
    if actor.poly or actor.circle then
        if actor.poly then
            actor.shape = self.hc:addPolygon( unpack(actor.poly)   )
        else
            actor.shape = self.hc:addCircle ( unpack(actor.circle) )
        end
        
        actor.shape:moveTo(actor.x, actor.y)
        actor.shape.actor = actor
        if actor.passive then self.hc:setPassive(actor.shape) end
    end
end

function ActorManager:add_player(player)
    player:move(Conf.width / 2, Conf.height - 50)
    self:add(player)
    self.player = player
    player.mng  = self
end
