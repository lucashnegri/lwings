Actions = {}

function Actions.exp_tiny(self)
    Actor.spawn(self.mng, 'exp_tiny', self.x, self.y)
    Audio.effect('hit', 0.5)
end

function Actions.exp_big(self)
    Actor.spawn(self.mng, 'exp_big', self.x, self.y)
    Audio.effect('explosion_big')
end

function Actions.explode_small(self)
    if self.killed then
        Actor.spawn(self.mng, 'exp_small', self.x, self.y)
        Audio.effect('explosion')
    end
    
    return Actor.death(self, mng)
end

function Actions.explode_big(self)
    if self.killed then Actions.exp_big(self) end    
    
    return Actor.death(self)
end

function Actions.shoot_simple(self, dt)
    if self.cd <= 0 then    
        Bullet.shoot_dir(self.mng, 'enemy_bullet', self.x, self.y + self.gun_y, 0, 1)
        self.cd = self.cdv
    else
        self.cd = self.cd - dt
    end
end

function Actions.shoot_player(self, dt)
    if self.cd <= 0 then
        local tg = self.mng.player
        Bullet.shoot_target_dir(self.mng, 'enemy_bullet2', self.x, self.y + self.gun_y, tg.x, tg.y)
        self.cd = self.cdv
    else
        self.cd = self.cd - dt
    end
end

function Actions.desapear(self, dt)
    if self.y > (Conf.height + self.anim.h) then
        self.alive = false
    end
    
    return Actor.update(self, dt)
end
