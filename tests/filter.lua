local Utils = require('utils')

local bag = {
    {z = 3},
    {z = 4},
    {z = 5},
    {z = 3},
    {z = 2},
    {z = 5},
    {z = 3},
}

Utils.filter(bag, 'z', 3)
assert(#bag == 4)
for i, j in ipairs(bag) do
    assert(j.z ~= 3)
end
