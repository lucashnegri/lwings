local Utils = require('utils')

local rect1 = {-100, 100, -100, 100}
local rect2 = { -50,  50,  -50,  50}
local rect3 = { 101,  150, -50,  50}
local rect4 = { 100,  101, -50,  50}
local rect5 = { 0,  300,   -100,  -51}
assert(     Utils.rect_collision(rect1, rect2) )
assert(     Utils.rect_collision(rect2, rect1) )
assert( not Utils.rect_collision(rect1, rect3) )
assert( not Utils.rect_collision(rect2, rect3) )
assert(     Utils.rect_collision(rect3, rect4) )
assert( not Utils.rect_collision(rect3, rect5) )

assert(     Utils.inside_rect(0, 0, rect1)      )
assert(     Utils.inside_rect(100, -100, rect1) )
assert( not Utils.inside_rect(101, 0, rect1)    )
assert( not Utils.inside_rect(-101, 0, rect1)   )
assert(     Utils.inside_rect(50, -56, rect1)   )
assert( not Utils.inside_rect(26, 200, rect1)   )
