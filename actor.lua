local ctrl = Utils.control

Actor = {}
local ActorMT = {__index = Actor}

function Actor.new(conf)
    local self = Utils.shallow_copy(conf)
    setmetatable(self, ActorMT)
    
    self.alive  = true
    self.cname  = self.cname or 'other'
    
    local a     = self.anim
    self.anim   = a and Animation.new(a.name, a.n, a.dur, a.fix, a.back)
    self.script = Script.new(self.script)
    
    if self.cdv then self.cd = self.cdv end
    
    return self
end

function Actor:update(dt)
    self.tacc = self.tacc + dt
    
    -- verify if it is alive (could be killed or could run away)
    if self.tmax and self.tacc > self.tmax then
        self.alive  = false
        self.killed = false
    elseif self.life and self.life <= 0 then
        self.alive  = false
        self.killed = true
    end
    if not self.alive then return false end
    
    -- update current state
    if self.anim then self.sprite = self.anim:update(dt) end
    local move  = self.script:update(dt)
    
    -- move
    if move then
        local nx, ny = self.x, self.y
    
        if move.full then
            nx = self.x + self.x_speed * dt
            ny = self.y + self.y_speed * dt
        else
            nx = ctrl(self.x, move.x, self.x_speed * dt)
            ny = ctrl(self.y, move.y, self.y_speed * dt)
        end
        
        self:move(nx, ny)
    end
    
    return true
end

function Actor:move(x, y)
    self.x = x
    self.y = y
    
    if self.shape then self.shape:moveTo(self.x, self.y) end
end

function Actor:draw()
    --[[ shows the bounding polygon (debug)
    love.graphics.setColor(255,255,255,255)
    if self.shape then self.shape:draw(line) end
    --]]
    
    if not self.anim then return end
    
    local x = self.x - self.anim.adj_x
    local y = self.y - self.anim.adj_y
    love.graphics.draw(self.sprite, x, y)
end

function Actor:collision(other)
end

function Actor:damage(qt)
    if self.life then
        self.life = self.life - qt
    end
    
    return true
end

function Actor:death()
    if self.killed and self.points then
        self.mng.player:add_score(self.points)
    end
    
    if self.drop and math.random() < self.drop then
        Actor.spawn(self.mng, 'power_up', self.x, self.y)
    end
end

local cache = {}

function Actor.spawn(mng, name, x, y)
    if not cache[name] then
        cache[name] = require('objs/' .. name)
    end
    
    local act = Actor.new(cache[name])
    act.name  = name
    act:move(x, y)
    act.tacc  = 0
    
    if mng then
        act.mng = mng
        mng:add(act)
    end
    
    return act
end
